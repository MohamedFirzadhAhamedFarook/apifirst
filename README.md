# APIFirst

### Prerequisite

Setup local maven repo so that it can be used as a local repository

---

A simple POC to depict a workflow for the API first approach.
To run use the command `mvn install`